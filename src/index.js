document.getElementById('nav').onmouseover = function(event) {
    var target = event.target;
    if (target.className == 'menu-item') {
        var s = target.getElementsByClassName('submenu');
        closeMenu();
        s[0].style.display = 'block';
    }
}
document.onmouseover = function (event) {
    var target = event.target;
    console.log(event.target);
    if (target.className != 'menu-item' && target.className != 'submenu') {
        closeMenu();
    }
}
function closeMenu() {
    var menu = document.getElementById('nav');
    var subm = document.getElementsByClassName('submenu');
    for (var i = 0; i <subm.length; i++) {
        subm[i].style.display = 'none';
    }
}

function fun1() {
    var checkbox_one;       //переменная
    checkbox_one = document.getElementsById('checkbox-one');     //нашол по ID
    if (checkbox_one.checked) {     //checked состаяние элемента
        alert('вибран')
    }
    else {
        alert('не выбран')
    }
};
function fun2() {
    var radi = document.getElementsByName('r1');      //radi переменная
    for (var i = 0 ; i < radi.length; i++) {
        if (radi[i].checked) {
            alert('выбран' +i+ 'элемент');
        }
    }
};
function fun3() {
    var sel = document.getElementById('select-one').selectedIndex;  //sel переменная
    var options1 = document.getElementById('select-one').options;

    alert('выбрана опция' +options1[sel].text);
}
function fun4() {
    var range_one = document.getElementById('r50');
    var p_one = document.getElementById('range-one');
    p_one.innerHTML = range_one.value; 
}
function fun5() {
    var range_two = document.getElementById('r50-two');
    var input_range = document.getElementById('range-two');
    input_range.value = range_two.value; 
}
function fun6() {
    var range_three = document.getElementById('r50-three');
    var div_range = document.getElementById('range-three');
    div_range.style.width = range_three.value + 'px'; 
}

function fun7() {
    var rtl = document.getElementById('rtl');
    var rtr = document.getElementById('rtr');
    var rbr = document.getElementById('rbr');
    var rbl = document.getElementById('rbl');
    var ttl = document.getElementById('ttl');
    var ttr = document.getElementById('ttr');
    var tbr = document.getElementById('tbr');
    var tbl = document.getElementById('tbl');

    var box_3_block = document.getElementById('box-3_block');

    ttl.value = rtl.value;
    ttr.value = rtr.value;
    tbr.value = rbr.value;
    tbl.value = rbl.value;

    box_3_block.style.borderRadius = rtl.value + 'px ' + rtr.value + 'px ' + rbr.value + 'px ' + rbl.value + 'px '; //пробелы в "px " обезательно

}